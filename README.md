# Dataset Registry

The Dataset Registry holds all the datasets tracked by DVC

To list all the datasets tracked by this registy use:
```bash
dvc list https://gitlab.com/rafay.ahmad/dataset-registry 
```

To simply download a dataset from registry:
```bash
dvc import https://gitlab.com/rafay.ahmad/dataset-registry \
           required/dataset/data
```

Import a dataset also saves its registry link allowing you to keep up to data with the registry without having to download the dataset again, by simply using `dvc update required/dataset/data.dvc`

To import a dataset from registry to your project:
```bash
dvc import https://gitlab.com/rafay.ahmad/dataset-registry \
           required/dataset/data 
```